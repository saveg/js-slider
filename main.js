window.onload = function(){

    const imagesBox = document.querySelectorAll('.image-box');
    const arrowLeft = document.querySelector('.myarrowleft');
    const arrowRight = document.querySelector('.myarrowright');
    let current = 0;

    // Clear all images boxes
    function reset(){

        for(let i = 0; i < imagesBox.length; i++){

            imagesBox[i].style.display = 'none';

        }

    }

    // Initialize Slider
    function startSlide(){

        reset();
        imagesBox[0].style.display = 'block';

    }

    // Show prev Image box
    function slideLeft(){

        reset();
        imagesBox[current - 1].style.display = 'block';
        current--;

    }

    // Show next Image box
    function slideRight(){

        reset();
        imagesBox[current + 1].style.display = 'block';
        current++;

    }

    // Left arrow click
    arrowLeft.addEventListener('click', function(){

        if(current === 0){
            current = imagesBox.length;
        }
        slideLeft();

    });

    // Right arrow click
    arrowRight.addEventListener('click', function(){

        if(current === imagesBox.length - 1){
            current = -1;
        }
        slideRight();

    });

    startSlide();


}